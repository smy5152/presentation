function Quiz1Submit() {
        var ans1 = document.getElementById("nice").checked;
        var ans2 = document.getElementById("view").checked;
        var ans3 = document.getElementById("changes").checked;
        var ans4 = document.getElementById("dance").checked;

	if ((ans1 == false) && (ans2 == true) && (ans3 == false) && (ans4 == false)) {
		document.getElementById("dance1").innerHTML = "Well done";
		document.getElementById("Quiz1Background").src= "images/Onigiri_cheerleader.gif";
	}else{
		document.getElementById("dance1").innerHTML = "Reset and try again";
	}
}

function Quiz1Reset() {
        document.getElementById("nice").checked = false;
        document.getElementById("view").checked = false;
        document.getElementById("changes").checked = false;
        document.getElementById("dance").checked = false;
	document.getElementById("dance1").innerHTML = "";
	document.getElementById("Quiz1Background").src= "images/michael-dziedzic-unsplash.jpg";
}

function Quiz3Submit() {
        var ans1 = document.getElementById("customery").checked;
        var ans2 = document.getElementById("taxn").checked;
        var ans3 = document.getElementById("coronay").checked;
        var ans4 = document.getElementById("historyy").checked;

        if ((ans1 == true) && (ans2 == true) && (ans3 == true) && (ans4 == true)) {
                document.getElementById("dance3").innerHTML = "Well done";
		document.getElementById("Quiz3Background").src= "images/Zhu_zahiyu_flagdance.gif";
        }else{
                document.getElementById("dance3").innerHTML = "Reset and try again";
        }
}

function Quiz3Reset() {
        document.getElementById("customery").checked = false;
        document.getElementById("customern").checked = false;
        document.getElementById("taxy").checked = false;
        document.getElementById("taxn").checked = false;
        document.getElementById("coronay").checked = false;
        document.getElementById("coronan").checked = false;
        document.getElementById("historyy").checked = false;
        document.getElementById("historyn").checked = false;
	document.getElementById("dance3").innerHTML = "";
	document.getElementById("Quiz3Background").src= "images/michael-dziedzic-unsplash.jpg";
}
